image: maven:3-jdk-8-alpine

variables:
  MAVEN_CLI_OPTS: "-s .ci/settings.xml --batch-mode"
  MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
    - .m2/repository/
    - target/

stages:
  - compile
  - test
  - deploy

compile:
  stage: compile
  script:
    - mvn $MAVEN_CLI_OPTS -U clean compile  # re-fetch snapshots every time

test:jdk8:
  stage: test
  image: maven:3-jdk-8
  except:
    - master@tinyMediaManager/tinyMediaManager
  only:
    - pushes
  script:
    # install libmediainfo for unit tests
    - apt-get update && apt-get install -y --no-install-recommends libmms0
    - wget https://mediaarea.net/download/binary/libzen0/0.4.37/libzen0v5_0.4.37-1_amd64.Debian_9.0.deb
    - dpkg -i libzen0v5_0.4.37-1_amd64.Debian_9.0.deb
    - wget https://mediaarea.net/download/binary/libmediainfo0/18.12/libmediainfo0v5_18.12-1_amd64.Debian_9.0.deb
    - dpkg -i libmediainfo0v5_18.12-1_amd64.Debian_9.0.deb
    - mvn $MAVEN_CLI_OPTS -DskipTests=false -DskipITs=true -Dmaven.test.failure.ignore=false test

test:jdk10:
  stage: test
  image: maven:3-jdk-10
  except:
    - master@tinyMediaManager/tinyMediaManager
  only:
    - pushes
  script:
    # install libmediainfo for unit tests
    - apt-get update && apt-get install -y --no-install-recommends libmms0
    - wget https://mediaarea.net/download/binary/libzen0/0.4.37/libzen0v5_0.4.37-1_amd64.Debian_9.0.deb
    - dpkg -i libzen0v5_0.4.37-1_amd64.Debian_9.0.deb
    - wget https://mediaarea.net/download/binary/libmediainfo0/18.12/libmediainfo0v5_18.12-1_amd64.Debian_9.0.deb
    - dpkg -i libmediainfo0v5_18.12-1_amd64.Debian_9.0.deb
    - mvn $MAVEN_CLI_OPTS -DskipTests=false -DskipITs=true -Dmaven.test.failure.ignore=false test

test:jdk11:
  stage: test
  image: maven:3-jdk-11
  except:
    - master@tinyMediaManager/tinyMediaManager
  only:
    - pushes
  script:
    # install libmediainfo for unit tests
    - apt-get update && apt-get install -y --no-install-recommends libmms0
    - wget https://mediaarea.net/download/binary/libzen0/0.4.37/libzen0v5_0.4.37-1_amd64.Debian_9.0.deb
    - dpkg -i libzen0v5_0.4.37-1_amd64.Debian_9.0.deb
    - wget https://mediaarea.net/download/binary/libmediainfo0/18.12/libmediainfo0v5_18.12-1_amd64.Debian_9.0.deb
    - dpkg -i libmediainfo0v5_18.12-1_amd64.Debian_9.0.deb
    - mvn $MAVEN_CLI_OPTS -DskipTests=false -DskipITs=true -Dmaven.test.failure.ignore=false test

#test:jdk12:
#  stage: test
#  image: maven:3-jdk-12
#  except:
#    - master
#  script:
#    # install libmediainfo for unit tests
#    - apt-get update && apt-get install -y --no-install-recommends libmms0
#    - wget https://mediaarea.net/download/binary/libzen0/0.4.37/libzen0v5_0.4.37-1_amd64.Debian_9.0.deb
#    - dpkg -i libzen0v5_0.4.37-1_amd64.Debian_9.0.deb
#    - wget https://mediaarea.net/download/binary/libmediainfo0/18.12/libmediainfo0v5_18.12-1_amd64.Debian_9.0.deb
#    - dpkg -i libmediainfo0v5_18.12-1_amd64.Debian_9.0.deb
#    - mvn $MAVEN_CLI_OPTS -DskipTests=false -DskipITs=true -Dmaven.test.failure.ignore=false test
#  allow_failure: true

deploy:nightly:
  stage: deploy
  image: maven:3-jdk-8                                # stay with stretch
  only:
    - devel@tinyMediaManager/tinyMediaManager
  except:
    - pushes                                          # do not run packaging on push-builds
  script:
    # update package sources and install ant, 32 bit libs, git and lftp
    - dpkg --add-architecture i386 && apt-get update && apt-get install -y --no-install-recommends ant libstdc++6:i386 libgcc1:i386 zlib1g:i386 libncurses5:i386 git lftp curl
    # generate changelog.txt for nightly builds
    - ./generate_changelog.sh
    # package
    - mvn $MAVEN_CLI_OPTS -P gitlab-ci -DbuildNumber=${CI_COMMIT_SHA:0:8} -Dgetdown=getdown-nightly.txt -Dthread_pool_size=1 package

    # deploy also to old HP until we've fully migrated to the new one
    # push the build to the webserver
    - lftp -c "set ftp:ssl-allow no; open -u ${FTP_USER_NIGHTLY},${FTP_PASSWORD_NIGHTLY} ${FTP_HOST}; mirror -Rev build/ ./build; mirror -Rev dist/ ./dist;"
    # and publish the files on the webserver
    - curl http://nightly.tinymediamanager.org/bin/publish_build_v3.php

    # trigger the deployment on nightly.tinymediamanager.org (new HP)
    - curl -X POST -F "token=${NIGHTLY_PIPELINE_TOKEN}" -F "ref=master" -F "variables[JOB_ID]=${CI_JOB_ID}" https://gitlab.com/api/v4/projects/7895208/trigger/pipeline

  artifacts:
    paths:
      - build/
      - dist/
    expire_in: 1 week

deploy:prerel:
  stage: deploy
  image: maven:3-jdk-8  # stay with stretch
  only:
    - master@tinyMediaManager/tinyMediaManager
  when: manual
  script:
    # update package sources and install ant + 32 bit libs
    - dpkg --add-architecture i386 && apt-get update && apt-get install -y --no-install-recommends ant libstdc++6:i386 libgcc1:i386 zlib1g:i386 libncurses5:i386 lftp curl
    # package
    - mvn $MAVEN_CLI_OPTS -P gitlab-ci -DbuildNumber=${CI_COMMIT_SHA:0:8} -Dgetdown=getdown-prerelease.txt -Dthread_pool_size=1 package

    # deploy also to old HP until we've fully migrated to the new one
    # push the build to the webserver
    - lftp -c "set ftp:ssl-allow no; open -u ${FTP_USER_PREREL},${FTP_PASSWORD_PREREL} ${FTP_HOST}; mirror -Rev build/ ./build; mirror -Rev dist/ ./dist;"
    # and publish the files on the webserver
    - curl http://prerelease.tinymediamanager.org/bin/publish_build_v3.php

    # trigger the deployment on nightly.tinymediamanager.org (new HP)
    - curl -X POST -F "token=${PREREL_PIPELINE_TOKEN}" -F "ref=master" -F "variables[JOB_ID]=${CI_JOB_ID}" https://gitlab.com/api/v4/projects/10869644/trigger/pipeline

  artifacts:
    paths:
      - build/
      - dist/

deploy:release:
  stage: deploy
  only:
    - master@tinyMediaManager/tinyMediaManager
  when: manual
  script:
    - echo 'ToDo'
