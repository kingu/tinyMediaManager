Version 3.0 - RC2
=======================

+ completely rewritten UI (thx to @joostzilla)
  - new style and layout (better usage of the available space - especially for low screen devices)
  - flexible and configurable tables
  - improved filters (inclusive and exclusive filters)
  - better UI scaling on high dpi screens
+ added a dark theme
+ removed donator version (but donations are still highly appreciated)
+ completely rewritten the renamer engine
+ completely rewritten NFO parsing and writing (much more flexible now)
+ easier translation via weblate.org now (https://hosted.weblate.org/projects/tinymediamanager/)
+ increased required Java version to Java 8+
+ ability to mix in missing episodes
+ presets in settings for common media centers
+ added a feature to find "unwanted" files for cleanup (e.g. .txt, .url, ...)
+ many enhancements under the hood
